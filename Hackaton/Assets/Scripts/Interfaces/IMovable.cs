﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMovable {

    // Use this for initialization
    void Move(Vector2 forceVector, Enums.ForceType forceType);
}
